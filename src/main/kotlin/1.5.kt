/*
1.5 D’String a MutableList<Char>
 */

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val palabra = scanner.nextLine()
    println(mutablelist(palabra))
}


fun mutablelist(palabra: String): MutableList<Char> {
    var lista = mutableListOf<Char>()
    for (i in palabra.indices){
        lista.add(palabra[i])
    }
    return (lista)
}