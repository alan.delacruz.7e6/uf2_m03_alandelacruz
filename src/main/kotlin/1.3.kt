/*
1.3 Caràcter d’un String
 */

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val palabra = scanner.nextLine()
    val posicio = scanner.nextInt()
    println(positionchar(palabra, posicio))
}


fun positionchar(palabra: String, posicio: Int): String {
    if (posicio > palabra.length) return ("La mida de l'String és inferior a $posicio")
    else return (palabra[posicio].toString())
}