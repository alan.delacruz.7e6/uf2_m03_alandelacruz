/*
1.10 Escriu un marc per un String
 */

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val paraula = scanner.nextLine()
    val caracter = scanner.nextLine()
    marcstring(paraula, caracter)
}

fun marcstring(paraula: String, caracter: String) {
    for (fila in 0..4){
        for (col in 0..paraula.length+3){
            if (fila == 0 || fila == 4){
                print(caracter)
            }
            else if (col == 0 || col == paraula.length+3){
                print(caracter)
            }
            else if (fila == 2){
                if (col >= 2 && col <= paraula.length +1){
                    print(paraula[col-2])
                }
                else{
                    print(" ")
                }
            }
            else{
                print(" ")
            }
        }
        println()
    }
}

