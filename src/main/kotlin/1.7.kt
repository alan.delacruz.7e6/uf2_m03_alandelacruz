/*
1.7 Eleva’l
 */

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val base = scanner.nextInt()
    val exponent = scanner.nextInt()
    println(calculpow(base, exponent))
}


fun calculpow(base: Int, exponent: Int): Int {
    return (Math.pow(base.toDouble(), exponent.toDouble()).toInt())
}