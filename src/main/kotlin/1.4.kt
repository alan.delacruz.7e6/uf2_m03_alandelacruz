/*
1.4 Subseqüència
 */

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val palabra = scanner.nextLine()
    val start = scanner.nextInt()
    val end = scanner.nextInt()
    println(positions(palabra, start, end))
}


fun positions(palabra: String, start: Int, end: Int): String {
    var guardar = ""
    if (end > palabra.length) return ("La subseqüència $start - $end de l'String no existe")
    else for (i in start until end){
          guardar += (palabra[i].toString())
    }
    return (guardar)
}