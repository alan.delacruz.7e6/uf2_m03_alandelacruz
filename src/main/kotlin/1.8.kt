/*
1.8 Escriu una línia
 */

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val espais = scanner.nextInt()
    val quantitat = scanner.nextInt()
    val caracter = scanner.next()
    println(writeoneline(espais, quantitat, caracter))
}


fun writeoneline(espais: Int, quantitat: Int, caracter: String): String {
    repeat(espais){
        print(" ")
    }
    repeat(quantitat){
        print(caracter)
    }
    return ""

}