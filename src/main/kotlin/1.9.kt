/*
1.9 Escriu una creu
 */

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val quantitat = scanner.nextInt()
    val caracter = scanner.next()
    crosschar(quantitat, caracter)
}

fun crosschar(quantitat: Int, caracter: String){
    for (filas in 0 until quantitat){
        for (col in 0 until quantitat){
            //1 a 1
            if (quantitat / 2 == col){
                print(caracter)
            }
            // varios
            else if (quantitat / 2 == filas){
                print(caracter)
            }
            else print(" ")
        }
        println()
    }

}


